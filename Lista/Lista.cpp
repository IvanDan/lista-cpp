#include "Lista.h"
#include "pch.h"
#include "framework.h"


namespace ListaLibrary {
  Lista::Lista() {
    dim = 2;
    count = 0;
    intPtr = new int[2];
  }

  Lista::~Lista() {
    delete[] intPtr;
  }

  void Lista::add(int num) {
    if (dim == count) {
      dim += 2;
      resize();
    }
    intPtr[count] = num;
    count++;
  }

  void Lista::remove(int index) {
    if (index == count - 1) {
      intPtr[index] = NULL;
    }
    else {
      intPtr[index] = NULL;
      for (int i = index; i < count - 1; i++) {
        swap(i, i + 1);
      }
    }
    count--;
    if (dim - count == 2) {
      dim -= 2;
      resize();
    }
  }

  void Lista::resize() {
    int* newIntPtr = new int[dim];
    for (int i = 0; i < count; i++) {
      newIntPtr[i] = intPtr[i];
    }
    delete[] intPtr;
    intPtr = newIntPtr;
  }

  void Lista::swap(int indexA, int indexB) {
    int tmp = intPtr[indexA];
    intPtr[indexA] = intPtr[indexB];
    intPtr[indexB] = tmp;
  }

  void Lista::display() {
    cout << "[";
    for (int i = 0; i < count; i++) {
      i == count - 1 ? cout << intPtr[i] << "]" << endl : cout << intPtr[i] << ", ";
    }
  }
}