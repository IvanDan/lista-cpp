#pragma once

#include <iostream>

using namespace std;

namespace ListaLibrary {
  class Lista
  {
  public:
    // costruttore 
    Lista();
    // distruttore
    ~Lista();
    // aggiunge i valori nella lista
    void add(int num);
    // rimuove un valore a un determinato indice
    void remove(int index);
    // stampa la lista
    void display();

  private:
    int dim; // la dimensione attuale della lista
    int count; // il numero di elementi nella lista
    void swap(int indexA, int indexB);
    void resize();

  protected:
    int* intPtr = nullptr; // il puntatore ai numeri
  };
}
